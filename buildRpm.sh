#!/bin/bash

rm -Rf socketcan 

if [ $# -ne 2 ]; then
	echo "Usage: "
	echo "./buildRpm.sh --tag <tag name>"
	echo "or"
	echo "./buildRpm.sh --revision <SVN revision number of trunk>"
	echo "We suggest using --revision 1277 as the latest reported working version"
	exit -1
elif [ $1 = "--tag" ]; then
	echo "This project, hosted at berlios, seems not to have to many tags (if it's changes just adapt the script"
	exit 1
#	echo "Building from tag: $2"
#	svn export https://svn.cern.ch/reps/atlas-pnikiel/VirtualElmbSimulator/tags/$2/ checkout
#	if [ $? -ne 0 ]; then
#		exit -1
#	fi
elif [ $1 = "--commit" ]; then
	echo "Building from revision: $2"
        git clone https://github.com/linux-can/can-utils.git
        cd can-utils
        git checkout $2
	if [ $? -ne 0 ]; then
		exit -1
	fi
        cd ..
else
	echo "--tag or --commit only allowed"
fi


rm -f rpmbuild/SOURCES/can-utils.tar
tar cf rpmbuild/SOURCES/can-utils.tar can-utils
rm -Rf socketcan
cd rpmbuild
rm -f SPECS/socketcan_utils.spec
# Fix the specfile so that the revision from which it was built is there
echo "%define version $2.0.0" > SPECS/socketcan_utils.spec
cat SPECS/socketcan_utils.spec.template >> SPECS/socketcan_utils.spec
rpmbuild -bb SPECS/socketcan_utils.spec
